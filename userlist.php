<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="stylesheet" href="http://restapi/styles.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <title>Hello API</title>
   </head>
   <body>
   <header>
  <nav>
    <h1>Hello<a target="_blank">API</a></h1>
    <ul>
      <li>
        <a href="http://restapi/index.html">Home</a>
      </li>
      <li>
        <a href="http://restapi/userlist.php">User list</a>
      </li>
    </ul>
  </nav>
  <nav id="small">
    <h1><a href="http://anaislab.com/" target="_blank">Hello</a>API</h1>
    <ul>
      <li>
        <a href="http://restapi/index.html">Home</a>
      </li>
      <li>
        <a href="http://restapi/userlist.php">User list</a>
      </li>
    </ul>
  </nav>
</header>
<div id="content">
<div id="container-book" class="container">
<!-- item user -->
  <div id="nousers">

       <h2>No users here yet! Add?<img src="../img/arrow.png"></h2>
  </div>
  <div style="visibility:hidden" class="row" id="content-wrapper">
  <input type="hidden" name="id" id="userid">
    <div class="col-sm-12">
      <div class="info-card"  data-name="john doe" data-job="Freelancer" data-location="jakarta">
        <div class="personal-detail-section">
          <div class="data">
          <ul>
            <li><h3><p id="name" >Name</p></h3></li>
            <li><h3><p id="lname">Lname</p></h3></li>
          </ul>
        </div>
        <div class="fields">
        <ul>
            <li><i class="fa fa-phone"></i><p id="phone"> +55 43287659</p></li>
            <li><i class="fa fa-twitter" ></i><p id="email">@johndoe</p></li>
        </ul>
        </div>
        <div class="btnblock">
            <ul>
              <li>
                   <div class="edit box" id="e" user="">
                 </div>
              </li>
              <li>
                  <div class="delete box" id="d" user="">
                  </div>
              </li>
            </ul>
        </div>
        </div>
      </div>
    </div>
  </div>
<!-- item user -->
  </div>
  <div>
      
      <a  id="add" class="btn btn-info btn-lg box" data-toggle="modal" data-target="#myModal">Open Modal</a>
  </div>
</div>

<!-- Modal -->

<div class="modal modal-material modal-center">
  <div class="modal-body"> 
    <h1 id="modal-header">Заголовок для MD окна</h1>
    <input type="text" name="name"  id="name" placeholder="name" />
    <input type="text" name="lname" id="lname" placeholder="lname" />
    <input type="text" name="email" id="email" placeholder="email" />
    <input type="text" name="phone" id="phone" placeholder="phone" />
  </div>
  <div class="modal-bottom">
    <ul>
      <li class="right"><a id="skip" href="#">CANCEL</a></li>
      <li class="right"><a id="submitpost" href="#" user="">OK</a></li>
    </ul>
  </div>
</div>
<!-- Modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
      function centerModal() {
          $el.css({
            'margin-top': -($el.outerHeight() / 2),
            'margin-left': -($el.outerWidth() / 2)
          });
        }
      function validateEmail(email) {
          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      }
      function validatePhoneNum(phone) {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        return phoneno.test(phone);
        }
    </script>
    <script>
     $.ajax({
          method: "GET",
          url: "http://restapi/users/",
          success: function(data) { 
              data = JSON.parse(data);
              if (data.length > 0){
                  var template =  document.getElementById("content-wrapper");
                  var container = document.getElementById("container-book");
                  template.style.visibility='visible';
                  template.querySelector("#name").innerHTML = data[0]['name'];
                  template.querySelector("#lname").innerHTML = data[0]['lname'];
                  template.querySelector("#phone").innerHTML = data[0]['phone'];
                  template.querySelector("#email").innerHTML = data[0]['email'];
                  template.querySelector("#userid").innerHTML = data[0]['id'];
                  template.querySelector("#e").setAttribute("user", data[0]['id']);
                  template.querySelector("#d").setAttribute("user", data[0]['id']);
                for (var i = 1; i < data.length; i++){
                     var cln = template.cloneNode(true);
                     container.appendChild(cln);
                     container.querySelector("#name").innerHTML = data[i]['name'];
                     container.querySelector("#lname").innerHTML = data[i]['lname'];
                     container.querySelector("#phone").innerHTML = data[i]['phone'];
                     container.querySelector("#email").innerHTML = data[i]['email'];
                     container.querySelector("#email").innerHTML = data[i]['email'];
                     container.querySelector("#userid").innerHTML = data[i]['id'];
                     container.querySelector("#e").setAttribute("user", data[i]['id']);
                     container.querySelector("#d").setAttribute("user", data[i]['id']);
                }
              }else{
                document.getElementById("nousers").style.display='block';
              }
          }
        });
    </script>
    <script>
       $(document).on('click', '.delete', function(e) { 
        let id = $(this).attr('user');
        $.ajax({
          method: "DELETE",
          url: 'http://restapi/users/'+id,
          success: function(data) { 
                // alert("deleted!");
          }
        });
        location.reload();
    });
    </script>

    <script>
    $(document).on('click', '.edit', function(e) {
        var id = $(this).attr('user');
        let uname = $('input#name');
        let ulname = $('input#lname');
        let uemail = $('input#email');
        let uphone = $('input#phone');

        $el = $('.modal-center');
                centerModal();
              $(window).resize(function() {
                centerModal();
              });
        $el.css("visibility", "visible");
        document.getElementById("modal-header").innerHTML = "EDIT USER";
        if( document.getElementById("submitpost")){
            document.getElementById("submitpost").setAttribute("user", id);
            document.getElementById("submitpost").setAttribute("id", "submitput");
        }

       var container = this.parentNode.parentNode.parentNode.parentNode;
       uname.val(container.querySelector("#name").innerHTML);
       ulname.val(container.querySelector("#lname").innerHTML);
       uemail.val(container.querySelector("#email").innerHTML);
       uphone.val(container.querySelector("#phone").innerHTML);
       uname = uname.val().toString();
       ulname = ulname.val().toString();
       uemail = uemail.val().toString();
       uphone = uphone.val().toString();

      
      }); 
    $(document).on('click', '#submitput', function(e) {

        let uname = $('input#name').val();
        let ulname = $('input#lname').val();
        let uphone = $('input#phone').val();
        let uemail = $('input#email').val();

       if(!validateEmail(uemail) || uemail == ''){
         alert('Email is not valid!');
         return;
       }else{
            uemail = uemail.toString();
       }
       if( ulname == '' || uname == '' || uphone == ''){
            alert('Fields must not be empty!');
             return;
         return;
       }else if(!validatePhoneNum(uphone)){
            alert('Wrong phone number! Example: 0938888888');
             return;
       }else{
        ulname = ulname.toString();
        uname = uname.toString();
        uphone = uphone.toString();
       }

        let id = $(this).attr('user');

        dataarr = {id:id, name:uname,lname:ulname, phone:uphone, email:uemail, deleted:"0"};
        
        $.ajax({
          method: "PUT",
          url: 'http://restapi/users/'+id,
          data: JSON.stringify(dataarr),
          success: function(data) { 
                alert("edited!");
          }
        });
        location.reload();
    });
    </script>
    <script>
    $(document).on('click', '#add', function(e) {
        $el = $('.modal-center');
        centerModal();
      $(window).resize(function() {
        centerModal();
      });

      $el.css("visibility", "visible");
      if( document.getElementById("submitput")){
         document.getElementById("submitput").setAttribute("id", "submitpost");
      }
     
      
      document.getElementById("modal-header").innerHTML = "ADD USER";
});
        $(document).on('click', '#submitpost', function(e) {
            let uname = $('input#name').val();
            let ulname = $('input#lname').val();
            let uemail = $('input#email').val();
            let uphone = $('input#phone').val();
            if(!validateEmail(uemail) || uemail == ''){
                 alert('Email is not valid!');
                 return;
               }else{
                    uemail = uemail.toString();
               }
               if( ulname == '' || uname == '' || uphone == ''){
                    alert('Fields must not be empty!');
                    return;
               }else if(!validatePhoneNum(uphone)){
                    alert('Wrong phone number!');
                     return;
               }else{
                ulname = ulname.toString();
                uname = uname.toString();
                uphone = uphone.toString();
               }

            var dataarr = {name:uname,lname:ulname, phone:uphone, email:uemail, deleted:"0"};
            $.ajax({
              method: "POST",
              url: 'http://restapi/users/',
              data: JSON.stringify(dataarr),
              success: function(data) { 
                    // alert("posted!");
                    $(".modal-center input[type='text']").val("");
                    $el.css("visibility", "hidden")

              }
            });
            location.reload();
        });
        $(document).on('click', '#skip', function(e) {
            $(".modal-center input[type='text']").val("");
            $('.modal-center').css("visibility", "hidden");
        });


    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   </body>
</html>