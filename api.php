<?php
  ini_set('display_errors', true);
  error_reporting(E_ERROR);

// получаем http метод
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);
 

// конектимся к бд
$link = mysqli_connect('localhost', 'root', '', 'phonebook');

mysqli_set_charset($link,'utf8');
 
// получаем таблицу и ключ из пути
$table = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));
$key = array_shift($request)+0;
 

$columns = preg_replace('/[^a-z0-9_]+/i','',array_keys($input));
$values = array_map(function ($value) use ($link) {
  if ($value === null) return null;
  return mysqli_real_escape_string($link,(string)$value);
},array_values($input));
 
// строим SET для SQL запроса
$set = '';
for ($i=0;$i<count($columns);$i++) {
  $set.=($i>0?',':'').'`'.$columns[$i].'`=';
  $set.=($values[$i]===null?'NULL':'"'.$values[$i].'"');
}

$var = $_SERVER['PATH_INFO'];
//на случай ресурса search (каждый след. ресур уточняет предыдущий, в словах ресурсов REST рекомендуют использовать имена существ. а не глаголы, т.к. search - action, не очень корректно) но делала по т.з.
if ($method == 'GET' &&  strpos($var, 'search') !== false) {
    $key = substr( strrchr( $var, '/search/' ), 1 );
    parse_str($key, $result);

    $sql .= "SELECT * FROM `$table` WHERE `deleted` = '0' ";
    foreach ($result as $key => $value) {
      $sql .= "$key LIKE '%$value%'".$and;
      if (count($result) > 1 && count($result) != $i){
        $sql .= " AND ";
      }
      $i++;
    }

}else{
  //если просто запросы - выдаем дату
  switch ($method) {
    case 'GET':
      $sql = "SELECT * FROM `$table` WHERE `deleted` = '0' ".($key?"  AND id=$key":''); break;
    case 'PUT':
      $sql = "UPDATE `$table` SET $set WHERE id=$key AND `deleted` = '0' "; break;
    case 'POST':
      $sql = "INSERT  INTO `$table` SET $set" ; break;
    case 'DELETE':
      $sql = "UPDATE `$table`  SET `deleted` = '1' WHERE id=$key"; break;
  }
}

 
// выполняем SQL запрос
$result = mysqli_query($link,$sql);
 
// умираем в случае ошибок
if (!$result) {
  http_response_code(404);
  die(mysqli_error());
}
 
// пишем что получилось
if ($method == 'GET') {
  if (!$key) echo '[';
  for ($i=0;$i<mysqli_num_rows($result);$i++) {
    echo ($i>0?',':'').json_encode(mysqli_fetch_object($result));
  }
  if (!$key) echo ']';
} elseif ($method == 'POST') {
  echo mysqli_insert_id($link);
} else {
  echo mysqli_affected_rows($link);
}
 
// закрываем соединение
mysqli_close($link);